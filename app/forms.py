from django import forms
from app.models import Question


class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput())
    password_repeat = forms.CharField(widget=forms.PasswordInput())

    def clean_username(self):
        username = self.cleaned_data['username']
        if username == 'Bob':
            self.add_error('username', 'wrong Bob shall not pass')
        return username

    def clean(self):
        cleaned_data = super().clean()
        password_one = cleaned_data['password']
        password_two = cleaned_data['password_repeat']
        if password_one != password_two:
            self.add_error(None, 'passwords do not match')
        return cleaned_data


class QuestionForm(forms.ModelForm):
    class Meta:
        model = Question
        fields = ['title', 'text']
