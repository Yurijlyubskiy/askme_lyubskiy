from django.shortcuts import render, redirect, Http404, reverse
from django.contrib import auth
from django import forms
from django.contrib.auth.decorators import login_required
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from app.models import Question
from app.models import Tag
from app.models import User
from app.models import Answer
from app.forms import LoginForm, QuestionForm


questions = [
    {
        'id': idx,
        'title': f'Title number {idx}',
        'text': f'Some text for question #{idx}'
    } for idx in range(30)
]


def paginate(objects_list, request, per_page=5):
    paginator = Paginator(objects_list, per_page)
    page = request.GET.get('page')
    iterators = paginator.get_page(page)
    return iterators


def hot_questions(request):
    contact_list = Question.objects.get_hot()
    iterators = paginate(contact_list, request, 5)
    return render(request, 'hot.html', {'iterators': iterators, 'url': "/new/"})


def new_questions(request):
    contact_list = Question.objects.get_new()
    iterators = paginate(contact_list, request, 5)
    return render(request, 'new.html', {'iterators': iterators,   'url': "/hot/"})


@login_required
def ask_question(request):
    if request.method == 'GET':
        form = QuestionForm()
    else:
        form = QuestionForm(data=request.POST)
        if form.is_valid():
            question = form.save(commit=False)
            question.fk_profile = request.user.profile
            question.save()
            return redirect(reverse('one_question', args=[question.pk]))

    return render(request, 'ask.html', {'form': form})


def signup(request):
    return render(request, 'register.html', {})


def mylogin(request):
    if request.method == 'GET':
        form = LoginForm
    else:
        form = LoginForm(data=request.POST)
        if form.is_valid():
            user = auth.authenticate(request, **form.cleaned_data)
            if user is not None:
                auth.login(request, user)
                request.session['hello'] = 'world'
                return redirect(reverse(new_questions))

    return render(request, 'login.html', {'form': form})


def settings(request):
    return render(request, 'settings.html', {})


def logout(request):
    print("SESSION SAY:" + request.session.pop('hello', 'nothing'))
    auth.logout(request)
    return redirect(reverse(new_questions))


def one_question(request, pk):
    question = Question.objects.get_by_id(pk=pk)
    mytags = Tag.objects.get_by_question(question)
    iterators = paginate(Answer.objects.get_by_question_id(pk), request, 5)
    return render(request, 'question.html', {'question': question, 'iterators': iterators, 'mytags': mytags})


def tags(request, tg):
    quest_list = Question.objects.get_by_tag(tg)
    iterators = paginate(quest_list, request, 5)

    return render(request, 'tag.html', {'iterators': iterators, 'header': tg, 'url': "/new/"})
